import { reactive, ref, type Ref } from 'vue';
import { defineStore } from 'pinia';
import axios, { Axios, AxiosError } from 'axios';

export interface CompactCityJson {
  "id": number,
  "name": string,
  "country": string,
};

interface SuccesResponse {
  type: "Succes"
  from: string
  temp: number
};

interface ErrorResponse {
  type: "Error"
  from: string
  message: string
}

export const useCityStore = defineStore('city', () => {
  const cities: Ref<CompactCityJson[]> = ref([]);
  

  async function fetchCities() {
    const response = await axios.get('http://localhost:8080/api/test')
    response.data.forEach((city: CompactCityJson) => {
      cities.value.push(city);
    });
  };

  function filterArray(input: string): CompactCityJson[] {
    input = formatCityName(input);
    return cities.value.filter((city) =>
      input.split('').every((char, index) => city.name[index] === char)
    ).slice(0, 10);
  };

  async function getTemperatureOfCity(cityName: string): Promise<SuccesResponse | ErrorResponse> {
    cityName = formatCityName(cityName)
    if (isCityNameValid(cityName)) {
      return fetchCityTemp(cityName)
    } else {
      return {
        type: "Error",
        from: "Validation",
        message: "Wrong City name"
      }
    }
  }

  async function fetchCityTemp(cityName: string): Promise<SuccesResponse | ErrorResponse> {
    try {
      const promise = await axios.get('http://localhost:8080/api/weather', {
        params: { city: cityName }
      });
      if (promise.status === 200) {
        return {
          type: "Succes",
          from: promise.data.from,
          temp: promise.data.temp
        }
      } else {
        return {
          type: "Error",
          from: "Backend",
          message: promise.data
        }
      }
    } catch (error) {
      return {
        type: "Error",
        from: "Frontend",
        message: error as string
      }
    }
  }

  function isCityNameValid(cityName: string) {
    const regex = /^[a-zA-Z\u00C0-\u024F\u1E00-\u1EFF\s\u2019']+$/u;
    return regex.test(cityName);
  }

  function formatCityName(cityName: string) {
    return cityName.trim().toLowerCase().charAt(0).toUpperCase() + cityName.slice(1);
  }
  return { fetchCities, filterArray, getTemperatureOfCity, isCityNameValid, formatCityName }
});
