import express from 'express';
import bodyParser from 'body-parser';
import router from "./appController";
import dotenv from 'dotenv'; 
import cors from 'cors';

dotenv.config();  // Load environment variables from .env file 

const app = express();


declare module 'express' {
    export interface Request {
            city?: string,
            temperature?: number,
    }
}

app.use(cors())
app.use(bodyParser.json());
app.use("/api", router());

export default app;