import fs from "fs"


interface CityJson {
    "id": number,
    "name": string,
    "state": string | null,
    "country": string,
    "coord": {
        "lon": number,
        "lat": number
    }
}

interface CompactCityJson {
    "id": number,
    "name": string,
    "country": string,
}


class CityProvider {
   
    private validCities!: CityJson[];
    private compactCities!: CompactCityJson[];

    constructor() {
        try {
            const data = fs.readFileSync('src/resources/city.list.json', { encoding: 'utf8' });
            // Assuming your file contains JSON data, parse it into an array of CityJson objects
            this.validCities = JSON.parse(data);
            this.mapValidCitiesForFrontend()
        } catch (err) {
            console.log(err);
            // Initialize the array in case of an error or if the file is empty
            this.validCities = [];
        }
    }

    public getValidCities(){
        return this.validCities;
    }
    public getCompactCities(){
        return this.compactCities;
    }

    private mapValidCitiesForFrontend(){
        this.compactCities = this.validCities.map((city)=>{
            return {id:city.id, name: city.name, country: city.country}
        })
    }
}

const cityProvider = new CityProvider()

export default cityProvider;