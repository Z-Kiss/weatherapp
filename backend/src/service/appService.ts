import express from 'express';
import { isCityExistInDatabase, getTemperature, setTemperature } from '../db/redis/redisClient';
import axios from 'axios';
import { saveLog } from '../db/mongo/weatherSchema';
import cityProvider from '../helper/cityProvider';

export async function getTemperatureOfCity(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        const city = req.city as string;

        if (await isCityExistInDatabase(city)) {
            const temperature = await getTemperature(city)

            return res.status(200).send({from:"Redis",temp:temperature});
        } else {
            return next()
        }

    } catch (error) {
        return res.status(400).send(`Internal Server Error: ${String(error)}`);
    }
}

export const gatherInformationFromAPI = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        const city = req.city as string;

        const response = await getInformationFromWeatherApi(city);

        if (response.status === 200) {

            await saveLog(response.data);
            req.temperature = extractTemperatureFromJsonResponse(response.data);

            return next();
        } else {
            return res.status(response.status).send(`Bad Request: ${response.data}`);
        }

    } catch (error) {

        return res.status(400).send(`Internal Server Error: ${String(error)}`);
    }
}

const getInformationFromWeatherApi = async (city: string) => {
    const { API_KEY } = process.env;

    return await axios.get("https://api.openweathermap.org/data/2.5/weather", {
        params: {
            q: city,
            appid: API_KEY,
            units: 'metric'
        }
    });
}

interface WeatherResponse {
    main: {
        temp: number;
    };
}

const extractTemperatureFromJsonResponse = (jsonResponse: WeatherResponse): number => {
    return jsonResponse.main.temp as number;
};

export const updateRedis = async (req: express.Request, res: express.Response,) => {
    try {
        const city = req.city as string;
        const temperature = req.temperature as number;

        await setTemperature(city, temperature)

        return res.status(200).send({from:"Mongo",temp: temperature});

    } catch (error) {

        return res.status(400).send(`Internal Server Error: ${String(error)}`);
    }
}

export const getCompactCityNames = (req: express.Request, res: express.Response,) =>{
    const compactCities = cityProvider.getCompactCities();

    if(compactCities){
        return res.status(200).json(compactCities);
    }else{
        return res.sendStatus(400);
    }

}
