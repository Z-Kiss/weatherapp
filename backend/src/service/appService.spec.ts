import { describe, expect, test } from '@jest/globals';
import { jestTest } from './appService';

describe('Jesting', () => {
    test('Test of jest with original func', () => {
        expect(jestTest()).toBe('original');
    });

    test('Test of jest with mocked func', () => {
        // Mock the module before requiring it
        jest.mock('./appService');
        
        // Access the mocked module
        const jestTestMocked = require('./appService') as { jestTest: jest.Mock };

        // Set up a mock implementation for the mocked function
        jestTestMocked.jestTest.mockReturnValue('mocked');

        // Call the mocked function
        expect(jestTestMocked.jestTest()).toBe('mocked');
    });
});

describe('getTemperatureOfCity',()=>{
    test('function works correct',()=>{
        
    })
})