import express from 'express'
import cityProvider from '../helper/cityProvider';

export const checkForCity = (req: express.Request, res: express.Response, next: express.NextFunction) =>{
    console.log('I got you')
    const city = req.query.city as string;
    
    if(!city){
        return res.status(400).send("City not presented");
    }else{
        req.city = city.toLowerCase().charAt(0).toUpperCase() + city.slice(1);
        return next();
    }
}

export const checkIfCityValid = async (req: express.Request, res: express.Response, next: express.NextFunction) =>{
    
    const city = req.city;
   
    if(city && isCityNameCorrect(city)){
        return next();
    }
    
    return res.status(404).send("City not found, check the name again!");
}

const isCityNameCorrect = (nameOfCity: string) =>{

    const validCities = cityProvider.getValidCities();

    for(const validCity of validCities){
        if(validCity.name === nameOfCity){
            return true
        }
    }
    
    return false
}
