import express from "express";
import { checkForCity, checkIfCityValid } from "./middleware/cityValidator";
import { gatherInformationFromAPI, getCompactCityNames, getTemperatureOfCity, updateRedis } from "./service/appService";


const router = express.Router();

export default (): express.Router => {
    router.get("/weather", checkForCity, checkIfCityValid, getTemperatureOfCity, gatherInformationFromAPI, updateRedis)
    router.get("/test",getCompactCityNames)
    return router;
}