import http from 'http';
import app from './app';
import connectToMongooDatabase from './db/mongo/mongodb';

connectToMongooDatabase()

const server = http.createServer(app);
server.listen(8080, () => {
    console.log('Server running on http://localhost:8080');})
    