import { Schema, model} from "mongoose";

const WeatherLogSchema = new Schema({
    timeStamp: { type: Date, default: Date.now },
    weatherLog: { type: Object },

});

const WeatherLogModel = model('WeatherLog', WeatherLogSchema);

/* export const getLogById = async (id: Types.ObjectId) =>{
    return await WeatherLogModel.find({_id: id});
}

export const getAllLog = () => WeatherLogModel.find() */

export const saveLog = async (weatherLog: Object): Promise<void> => {
      await new WeatherLogModel({ weatherLog: weatherLog }).save();
};
