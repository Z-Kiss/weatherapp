import mongoose, { Connection } from 'mongoose';

export default function connectToMongooDatabase(): void {

  const mongooseUrl = createMongooseUrl()

  const mongooseConnection = createConnection(mongooseUrl);

  mongooseConnection.on('error', (error) => {
    console.error(error)
  });

  mongooseConnection.once('open', () => {
    console.log("\nConnected to MongoDatabase\n")
  })
}

const createMongooseUrl = (): string => {

  const { MONGO_URL, MONGO_DATABASE, NODE_ENV } = process.env;

  if (MONGO_URL && MONGO_DATABASE && NODE_ENV) {

    if (NODE_ENV === "test") {
      return MONGO_URL + "test_database"
    }
    return MONGO_URL + MONGO_DATABASE

  } else {
    throw new Error("Missing environment variables")
  }
}

const createConnection = (mongoUrlToConnect: string): Connection => {
  mongoose.connect(mongoUrlToConnect);
  return mongoose.connection;
};

