import Redis, { RedisOptions } from "ioredis";



export default function createRedisClient(): Redis {

  const redisConfig = createRedisConfig();
  const redisClient = createClient(redisConfig);

  addRedisErrorHandler(redisClient);

  return redisClient;
}

const createRedisConfig = () => {

  const { REDIS_HOST, REDIS_PORT, NODE_ENV } = process.env;

  const config: RedisOptions = {
    host: REDIS_HOST,
    port: REDIS_PORT ? parseInt(REDIS_PORT, 10) : 6379, // Default to 6379 if REDIS_PORT is not set
  };

  if (NODE_ENV === 'test') {
    // Use the test Redis configuration
    return { ...config, db: 0 }
  } else {
    // Use the production Redis configuration
    return { ...config, db: 1 }
  }
}

const createClient = (config: RedisOptions) => {
  return new Redis(config);
}

const addRedisErrorHandler = (redis: Redis): void => {
  redis.on('error', (err: Error) => {
    console.error('\nRedis connection error:', err.message);
    redis.quit()
  });
}

const redis = createRedisClient();

export const isCityExistInDatabase = async (city: string): Promise<boolean> => {

  const existingRecordWithTheKey = await redis.exists(city);

  return existingRecordWithTheKey > 0;
}

export const getTemperature = async (city: string): Promise<string> => {

  const temperatureOfCity = await redis.get(city);

  if (temperatureOfCity) {
    return temperatureOfCity;
  } else {
    throw new Error("City not present, but should");
  }
}

export const setTemperature = async (city: string, temp: number): Promise<void> => {
  const { MINUTE_TO_PRESERVE_DATA_IN_REDIS } = process.env;
  redis.set(city, temp, 'EX', 60 * Number(MINUTE_TO_PRESERVE_DATA_IN_REDIS));
}